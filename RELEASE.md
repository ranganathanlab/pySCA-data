# Release Information

6.1: Bump version number for pySCA 6.1 release.

6.0: Split example data in [pySCA](https://github.com/reynoldsk/pySCA) into
     this separate repository.
