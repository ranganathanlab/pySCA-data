# pySCA data

> 09.2019
>
> Copyright (C) 2019 Olivier Rivoire, Rama Ranganathan, Kimberly Reynolds,
> Ansel George
>
> This program is free software distributed under the BSD 3-clause license,
> please see the file LICENSE for details.

Annotated sequences used as input for the examples in
[pySCA](https://github.com/ranganathanlab/pySCA/).
